import React from "react";
import {  SafeAreaView, StyleSheet, Text, View } from "react-native"
import { createStackNavigator } from 'react-navigation-stack';
import { RFValue } from "react-native-responsive-fontsize";
import QRCode from 'react-native-qrcode-svg';
import Ripple from 'react-native-material-ripple';



export default function confirmation({navigation }){
    return (
    <SafeAreaView style={styles.container}>
        <Text style={styles.confirmtext}>Confirmed...</Text>
        <View style={styles.container1}>
        <QRCode
          value={'Hello'}
          size={200}
          color="black"
          backgroundColor="white"
        />

        </View>
        <Text style={styles.details}>Pauls Creamery</Text>
        <Text style={styles.details1}><Text style={{fontWeight:'bold'}}>Date:</Text>06-08-2020</Text>
        <Text style={styles.details2}><Text style={{fontWeight:'bold'}}>Time:</Text>11AM</Text>





        <View style={{paddingTop:'10%'}}>
             <Ripple
                   onPress={() => console.log("see my bookings")}
                    style={styles.button}                   
             >                
                <Text style={{color:'#9378FF',fontSize:20}}> See My Bookings </Text>
            </Ripple>
        </View>
    </SafeAreaView>
  );
}






const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'#9279FE',
    justifyContent:'center',
    alignItems: 'center'
  },
  confirmtext: {
        color:'white',
        fontSize: RFValue(45,720),
        textAlign: 'center',
        fontWeight: 'bold',
        paddingLeft: '20%',
        paddingRight: '20%',
        paddingBottom: '10%',
        letterSpacing: 1
  },
  container1: {
      backgroundColor:'white',
      width:250,
      height:250,
      justifyContent:'center',
      alignItems: 'center',


  },
button: {
    alignItems: 'center',
    backgroundColor: 'white',
    width: 300,
    borderRadius:30,
    padding: 15,
  },
  details: {
    color:'white',
    fontSize: RFValue(30,720),
    textAlign: 'center',
    fontWeight: 'bold',
    paddingTop: '10%',
    paddingLeft: '10%',
    paddingRight: '10%',
    paddingBottom: '5%',
},
details1: {
    color:'white',
    fontSize: RFValue(22,720),
    textAlign: 'center',
    paddingLeft: '10%',
    paddingRight: '10%'
},
details2: {
    color:'white',
    fontSize: RFValue(22,720),
    textAlign: 'center',
    paddingTop: '5%',
    paddingLeft: '10%',
    paddingRight: '10%',
},
});