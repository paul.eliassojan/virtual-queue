import React, { useState } from 'react';
import { StyleSheet, Text, View, Picker,TextInput, TouchableWithoutFeedback,TouchableOpacity, Keyboard, KeyboardAvoidingView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';

export default function Vendor_reg2() {
    const [selectedValue, setSelectedValue] = useState("java");
    return (
    <KeyboardAvoidingView
        behavior={Platform.OS == "ios"? "padding" : "height"}
        keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
        style={{flex:1}}
      >    
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
        <ScrollView>  
        <View style={styles.card}>   
        <View style={{alignItems:'center'}}>
            <Text style={styles.title}>Add Your Shop</Text>
        </View>
        <View style={{alignItems:'center',marginTop:50}}>
        <TextInput
            style={styles.input}
            placeholder='Shop Name'           
    />
    <View style={{width:'90%', borderWidth:1, borderColor:'gray',borderRadius:10, backgroundColor:'#FBEEFF',marginBottom:20}}>
        <Picker
        selectedValue={selectedValue}
        style={styles.picker}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}>      
        <Picker.Item label="Choose Category" value="Choose Category" />
        <Picker.Item label="Bakery" value="bakery" />
        <Picker.Item label="Supermarket" value="supermarket" />
        <Picker.Item label="ATM" value="atm" />
        <Picker.Item label="Stationary Store" value="ss" />
        <Picker.Item label="Jewllery" value="js" />

      </Picker>
      </View>
    <TextInput
            style={styles.input}
            placeholder='District'           
    />
    <TextInput
            style={styles.input}
            placeholder='Town'           
    />
        <TextInput
            style={styles.input}
            placeholder='Time Slot'           
    />
    </View>
    <View style={{alignItems:'center',paddingTop:50}}>
        <TouchableOpacity
            onPress={"onPressLearnMore"}
            style={styles.button}                   
             >                
        <Text style={{color:'white',fontSize:25}}> Submit </Text>
         </TouchableOpacity>
    </View>
     </View>
     </ScrollView>
        </View>
        </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white', 
        flex:1,
        width:'100%',
        height:'100%',
    },
    title: {
        //fontWeight:'bold',
        fontSize:29,
        color:'black',
        paddingTop:10,
      },
      input:{
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor:'#FBEEFF',
        width:'90%',
        //padding:'7%',
        height:60,
        fontSize:17,
        borderRadius:10,
        marginBottom:20,
        paddingLeft:10
      },
    button: {
        alignItems: 'center',
        backgroundColor: '#9378FF',
        padding:11,
        width:'70%',
        borderRadius:50,
      },
      card: {
        borderRadius: 40,
        elevation: 4,
        marginTop:100,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 10 },
        shadowColor: '#333',
        shadowOpacity: 0.3,
        shadowRadius: 2,
        //marginHorizontal:4 ,
        //marginVertical: 0,
        padding: 30,
        marginBottom:10

    },
    picker:{
        height: 60,  
        width: "90%",  
        color: '#344953',  
        justifyContent: 'center',       
    }
})