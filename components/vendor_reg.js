import React from 'react';
import { StyleSheet, Text, View, Linking,TextInput, TouchableHighlight,TouchableWithoutFeedback,TouchableOpacity, Keyboard, KeyboardAvoidingView} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import { Icon } from 'react-native-elements';
import Vendor_reg2 from '../components/vendor_reg2'
import Ripple from 'react-native-material-ripple';

export default function Vendor_reg({navigation}) {
    return (
    <KeyboardAvoidingView
        behavior={Platform.OS == "ios"? "padding" : "height"}
        keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
        style={{flex:1}}
      >    
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
        <ScrollView>  
        <View style={styles.card}>   
        <View style={{alignItems:'center'}}>
            <Text style={styles.title}>Create Your Account</Text>
        </View>
        <View style={{alignItems:'center',marginTop:50}}>
        <TextInput
            style={styles.input}
            placeholder='First Name'           
    />
        <TextInput
            style={styles.input}
            placeholder='Last Name'
            
    />
    <TextInput
            style={styles.input}
            placeholder='email'           
    />
    <TextInput
            style={styles.input}
            placeholder='password'           
    />
    </View>
    <View style={{alignItems:'flex-end',paddingTop:50}}>
        <Ripple
            onPress={() =>  navigation.navigate('Vendor_reg2')}
            style={styles.button}                   
             >                
        <Text style={{color:'white',fontSize:25}}> Next <Icon style={{height:19}} name="arrow-forward" type="ionicon" color="white" /></Text>
          
          </Ripple>
    </View>
     </View>
     </ScrollView>
        </View>
        </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white', 
        flex:1,
        width:'100%',
        height:'100%',
    },
    title: {
        //fontWeight:'bold',
        fontSize:29,
        color:'black',
        paddingTop:10,
      },
      input:{
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor:'#FBEEFF',
        width:'90%',
        //padding:'7%',
        height:60,
        fontSize:17,
        borderRadius:10,
        marginBottom:20,
        paddingLeft:10
      },
    button: {
        alignItems: 'center',
        backgroundColor: '#9378FF',
        padding:9,
        width:'35%',
        borderRadius:50,
      },
      card: {
        borderRadius: 40,
        elevation: 4,
        marginTop:100,
        backgroundColor: 'white',
        shadowOffset: { width: 1, height: 10 },
        shadowColor: '#333',
        shadowOpacity: 0.3,
        shadowRadius: 2,
        //marginHorizontal:4 ,
        //marginVertical: 0,
        padding: 30,
        marginBottom:10

    },
})