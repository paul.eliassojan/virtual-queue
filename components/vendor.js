import React from 'react';
import { StyleSheet, Text, View, Linking,TextInput, TouchableWithoutFeedback,TouchableOpacity, Keyboard, KeyboardAvoidingView} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'
import {ScrollView} from 'react-native-gesture-handler'
import Vendor_reg from '../components/vendor_reg'
import Admin from './screens/admin'
import Ripple from 'react-native-material-ripple';
export default function Vendor({ navigation }) {

    return (
        <KeyboardAvoidingView
        behavior={Platform.OS == "ios"? "padding" : "height"}
        keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
        style={{flex:1}}
      >
         
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
        <ScrollView>     
        <View style={styles.circle1}>
        </View>
        <View style={{paddingLeft:20}}>
            <Text style={styles.title}>Vendor</Text>
        </View>
        <View style={{alignItems:'center',marginTop:150}}>
        <TextInput
            style={styles.input}
            placeholder='email'           
    />
        <TextInput
            style={styles.input}
            placeholder='password'
            
    />
    </View>
    <View style={{alignItems:'center',paddingTop:50}}>
        <Ripple
            onPress={() =>  navigation.navigate('Admin')}
            style={styles.button}                   
             >                
        <Text style={{color:'#9378FF',fontSize:25,fontWeight:'bold'}}> Login </Text>
         </Ripple>
    </View>
    <View style={{alignItems:'center',paddingTop:30}}>
    <TouchableOpacity onPress={() =>  navigation.navigate('Vendor_reg')}>
        <Text style={{color:'white',fontSize:15}}>
            <Text>Dont have an account? </Text> 
            <Text style={{fontWeight:'bold',textDecorationLine: 'underline',}}>Signup</Text>
        </Text>
    </TouchableOpacity>
     </View>
     </ScrollView>
        </View>
        </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#9378FF', 
        flex:1,
        width:'100%',
        height:'100%'
    },
    title: {
        //fontWeight:'bold',
        fontSize:55,
        color:'white',
        paddingTop:50,
      },
      input:{
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor:'white',
        width:'74%',
        //padding:'7%',
        height:60,
        fontSize:17,
        borderRadius:10,
        marginBottom:20,
        paddingLeft:10
      },
    circle1: {
        position:'absolute',
        left:'71.13%',
        right:'-12.8%',
        top:'-8.29%',
        bottom:'87.98%',
        backgroundColor:'#D8D3EB',
        borderRadius:100
    },
    circle2:{
        position: 'absolute',
        left: '-12.8%',
        right: '71.13%',
        top: '87.98%',
        bottom: '-8.29%',
        backgroundColor: '#D8D3EB',
        borderRadius:100
    },
    button: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding:12,
        width:'70%',
        borderRadius:30,
        overflow:'hidden'
      },

})