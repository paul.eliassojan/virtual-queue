import React from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, Linking, Button, TouchableWithoutFeedback, Keyboard } from 'react-native';
import Vendor from '../components/vendor';
import Customer from '../components/customer';
import { createStackNavigator } from 'react-navigation-stack'
import Ripple from 'react-native-material-ripple';

export default function Login({ navigation }) {
    return (
            <View style={styles.container}>
            <View style={styles.circle1}>
            </View>
             <View style={{justifyContent:'center',alignItems:'center'}}>
                 <Text style={styles.title}>Are You</Text>
             </View>
             <View style={{justifyContent:'center',flex:0.8}}>
             <View style={{alignItems:'center'}}>
             <Ripple
                   onPress={() => navigation.navigate('Vendor')}
                    style={styles.button}                   
             >                
                <Text style={{color:'#9378FF',fontSize:25,fontWeight:'bold'}}> Vendor </Text>
            </Ripple>
            </View>
            <View style={{alignItems:'center'}}>  
            <Text style={{fontSize:55,paddingBottom:20,color:'white'}}>or</Text>
             <Ripple
             onPress={() => navigation.navigate('Customer')}
                    style={styles.button}                   
             >                
                <Text style={{color:'#9378FF',fontSize:25,fontWeight:'bold'}}> Customer </Text>
            </Ripple>
            </View>

            </View>
            <View style={styles.circle2}>
            </View>
            </View>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#9378FF',
        width: '100%',
        height: '100%',
    },
    title: {
        //fontWeight:'bold',
        fontSize:60,
        color:'white',
        textDecorationLine: 'underline',
        paddingTop:50,
      },
    button: {
        alignItems: 'center',
        backgroundColor: 'white',
        padding:13,
        width:'70%',
        borderRadius:30,
        marginBottom:10,
        overflow:'hidden'
      },
    circle1: {
        position:'absolute',
        left:'71.13%',
        right:'-12.8%',
        top:'-8.29%',
        bottom:'87.98%',
        backgroundColor:'#D8D3EB',
        borderRadius:100
    },
    circle2:{
        position: 'absolute',
        left: '-12.8%',
        right: '71.13%',
        top: '87.98%',
        bottom: '-8.29%',
        backgroundColor: '#D8D3EB',
        borderRadius:100
    }
})