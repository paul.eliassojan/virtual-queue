import React from 'react';
import QRCodeScanner from 'react-native-qrcode-scanner'
import {Linking, Text,TouchableOpacity,Alert} from 'react-native'

export default class Qr_scan extends React.Component{

    ifScanned=e =>{
        Linking.openURL(e.data).catch(err=>
            Alert.alert("Valid",e.data));
    }
    render(){
        return(
            <QRCodeScanner
                containerStyle={{backgroundColor:'#FFF'}}
                onRead={this.ifScanned}
                vibrate={false}
                reactivate={true}
                permissionDialogMessage="Need Permission to access camera"
                reactivate={true}
                reactivateTimeout={10}
                cameraStyle={[{height:300}]}
                showMarker={true}
                markerStyle={{borderColor:"#FFF",borderRadius:10}}
                bottomContent={
                    <TouchableOpacity>
                        <Text style={{fontSize:21,color:'rgb(0,122,255)'}}>
                            Scan QR
                        </Text>
                    </TouchableOpacity>
                }/>
        )
    }
}