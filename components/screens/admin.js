import React from 'react';
import { Button, View } from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Screen1 from './screen1'
import Qr_scan from './qr_scan'
import {CustomNav} from './customNav'
import Icon from 'react-native-vector-icons/Ionicons'

// const Screen1_stack=createStackNavigator();
// const Screen2_stack=createStackNavigator();
const Drawer = createDrawerNavigator();
const HomeStack = createStackNavigator();
const Qr_stack = createStackNavigator();

function HomeStackScreen({navigation}) {
  return (
    <HomeStack.Navigator screenOptions={{
        headerStyle:{
            backgroundColor:"#9378FF",
        },
        headerTintColor:'#fff',

    }}>
      <HomeStack.Screen 
        name="Profile"
        component={Screen1}
        options={{ title: 'Profile',
        headerLeft:()=>(
            <Icon.Button name="ios-menu"
            style={{paddingLeft:15}}
            size={30}
            backgroundColor="#9378FF" color="white" onPress={()=>navigation.openDrawer()}></Icon.Button>
        ) }}
      />
    </HomeStack.Navigator>
  );
}

function Qr_stackScreen({navigation}) {
  return (
    <Qr_stack.Navigator screenOptions={{
        headerStyle:{
            backgroundColor:"#9378FF",
        },
        headerTintColor:'#fff',

    }}>
      <Qr_stack.Screen
        name="qr"
        component={Qr_scan}
        options={{ title:'Qr Scanner',
         headerLeft:()=>(
             <Icon.Button name="ios-menu"
             size={30}
             style={{paddingLeft:15}}
             backgroundColor="#9378FF" color="white" onPress={()=>navigation.openDrawer()}></Icon.Button>
         ) }}
      />
    </Qr_stack.Navigator>
  );
}



export default function Admin() {
  return (
    <NavigationContainer>
      <Drawer.Navigator drawerContent={props=> <CustomNav {...props}/>} initialRouteName="Home">
        <Drawer.Screen name="Dashboard" component={HomeStackScreen} />
        <Drawer.Screen name="Qr-Scanner" component={Qr_stackScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}