import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default function Screen1({navigation}) {
    return (
        <View>
            <Text>Screen1</Text>
        </View>

    )
}
const styles = StyleSheet.create({
    circle: {
        /* Ellipse 18 */

        position: 'absolute',
        width: 204,
        height: 204,
        left: 229,
        top: -40,
        backgroundColor: '#eee'
    }
})