import React from 'react';
import { Button, View ,StyleSheet} from 'react-native';
import { DrawerContentScrollView,DrawerItem } from '@react-navigation/drawer';
import {Avatar,Title,Caption,Paragraph,Drawer,Text,TouchableRipple,Switch} from 'react-native-paper'
import Icon from 'react-native-vector-icons/Ionicons';
import Vendor from '../vendor'
export  function CustomNav(props){
    return(
        <View style={{flex:1}}>
        <DrawerContentScrollView {...props}>
            <View style={styles.drawerContent}>
              <View style={styles.userInfoSection}>
                  <View style={{marginLeft:15,marginTop:40}}>
                      <Title style={styles.title}>Hello, Paul</Title>
                      <Caption style={styles.caption}>Royal Bakery</Caption>
                  </View>
              </View>

              <Drawer.Section style={styles.drawerSection}>
              <DrawerItem
                icon={({color,size})=> (
                    <Icon 
                    name="home-outline"
                    color={color}
                    size={size}
                    />
                )}
                  label="Dashboard"
                  onPress={()=> {props.navigation.navigate('Dashboard')}}
                  />
             <DrawerItem
                icon={({color,size})=> (
                    <Icon 
                    name="qr-code-outline"
                    color={color}
                    size={size}
                    />
                )}
                  label="QR Scanner"
                  onPress={()=> {props.navigation.navigate('Qr-Scanner')}}
                  
                  />
             <DrawerItem
                icon={({color,size})=> (
                    <Icon 
                    name="settings-outline"
                    color={color}
                    size={size}
                    />
                )}
                  label="Settings"
                  onPress={()=> {}}
                  
                  />
              </Drawer.Section>

            </View>
            </DrawerContentScrollView>
            <Drawer.Section style={styles.bottomDrawerSection}>
                <DrawerItem
                icon={({color,size})=> (
                    <Icon 
                    name="log-out-outline"
                    color={color}
                    size={size}
                    />
                )}
                  label="Logout"
                  onPress={()=> {}}
                  />
            </Drawer.Section>
            <Caption style={{marginBottom:20,padding:10,paddingLeft:95,backgroundColor:'#e8e6e1',fontSize:14}}>Version: 1.0</Caption>
          </View>
    )
                }

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {
      paddingLeft: 20,
    },
    title: {
      fontSize: 25,
      marginTop: 3,
      fontWeight: 'bold',
    },
    caption: {
      fontSize: 18,
      lineHeight: 24,
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
      marginTop: 50,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    preference: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingVertical: 12,
      paddingHorizontal: 16
    },
});
