import React, { useState } from "react";
import DATA from './bakery.json';
import { FlatList, SafeAreaView, Image, StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native"
import { createStackNavigator } from 'react-navigation-stack'
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { SolidIcons } from "react-native-fontawesome";
import customer_info from '../components/customer_info';
import {Icon} from 'react-native-elements';

const Item = ({ item, onPress, style  }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, style]}>
    <Text style={styles.title}>{item.name}</Text>
    <View style={{flexDirection: 'row',}}>
      <Text style={{fontSize:20 , color:"#A8A8A8", flex:9}}>{item.place}</Text>
      <Icon name="chevron-forward-circle-outline" type="ionicon" color="gray" style={{flex:1}} />
    </View>
    
    
  </TouchableOpacity>
);

export default function shoplist({ navigation }){
  const [selectedId, setSelectedId] = useState(null);

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? "#D3D3D3" : "#FFFFFF";
    return (
      <Item
        item={item}
        onPress={() => {setSelectedId(item.id); navigation.navigate('customer_info',item);}}
        style={{ backgroundColor }}

      />
    );
  };

  return (

    <SafeAreaView style={styles.container}>
    <View style={styles.topbar}>
                <TouchableOpacity style={styles.buttonclick} onPress={() => navigation.navigate('Customer')}>
                    
                    <Image style={styles.backimage} source={require('../assets/Back.png')}/>
                    <Text style={styles.back}>Back</Text>
                    <Text style={styles.Title}>Bakery</Text>
                </TouchableOpacity>
            </View>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        extraData={selectedId}
      />
    </SafeAreaView>
  );
}






const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding:25,
    borderBottomWidth:1,
  },
  title: {
    fontSize: 28,
    fontWeight: "bold",
  },

  Title: {
    fontSize: 32,
    fontWeight: "bold",
    margin:'25%'
  },
  topbar:{
    flexDirection: 'row',
    height:'10%',
    backgroundColor:"#F2F2F2",
    alignItems:'center',
},


back:{
  fontSize:RFValue(16, 720),
  color:'grey',
  textAlign:'left',
  paddingLeft:'1%'
},
buttonclick:{
  flexDirection: 'row',
  alignItems:'center',
},

backimage:{
  marginLeft:'2%'
},
});


