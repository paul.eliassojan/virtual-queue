import React ,{ useState }from 'react';
import { StyleSheet,Modal, Text, View, Picker,TextInput,TouchableHighlight,TouchableWithoutFeedback,TouchableOpacity, Keyboard, KeyboardAvoidingView, Pressable} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Ripple from 'react-native-material-ripple';
import { Icon } from 'react-native-elements';
import shoplist from '../components/shoplist';
import customer_info from '../components/customer_info';

export default function Customer( {navigation} ) {
    const [modalVisible, setModalVisible] = useState(false);
    const [selectedValue, setSelectedValue] = useState("java");
    const [selectedValue2, setSelectedValue2] = useState("java");
    const [selectedValue3, setSelectedValue3] = useState("java");


    return (
        <KeyboardAvoidingView
        behavior={Platform.OS == "ios"? "padding" : "height"}
        keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
        style={{flex:1}}
      >    
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
        <ScrollView>  
        <View style={{backgroundColor:'#e3e3e3',paddingBottom:30,flexDirection:'row'}}>
            <Text style={styles.title}>Welcome Customer,</Text>
            <Icon size={35} style={{paddingTop:40,paddingLeft:50}} name="options-outline" type="ionicon" color="gray"/>
        </View>
        <View style={{alignItems:'center'}}>
        <View style={styles.search}>
        <Icon name="location-outline" type="ionicon" />
        <TextInput
            style={styles.input}
            placeholder=' Enter Shop Name...'           
        />
       </View>
       </View>
       <View style={{paddingTop:30,}}>
            <Text style={{fontSize:40,paddingLeft:30,fontWeight:'bold'}}>Or,</Text>
        </View>
       
       <View style={{ borderColor: 'gray',borderTopWidth:2, borderBottomWidth: 2,height:100,alignItems:'center',justifyContent:'center',marginTop:20}}>        
        <Picker
        selectedValue={selectedValue}
        style={styles.picker}
        onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}>  

        <Picker.Item label="Choose your District" value="Choose the District" />
        <Picker.Item label="Bakery" value="bakery" />
        <Picker.Item label="Supermarket" value="supermarket" />
        <Picker.Item label="ATM" value="atm" />
        <Picker.Item label="Stationary Store" value="ss" />
        <Picker.Item label="Jewllery" value="js" />
      </Picker>
      </View>

      <View style={{borderBottomColor: 'gray', backgroundColor:'#e1e4e8',borderBottomWidth: 2,height:100,alignItems:'center',justifyContent:'center'}}>        
        <Picker
        selectedValue={selectedValue2}
        style={styles.picker}
        onValueChange={(itemValue, itemIndex) => setSelectedValue2(itemValue)}>  

        <Picker.Item label="Choose your area" value="Choose your area" />
        <Picker.Item label="Bakery" value="bakery" />
        <Picker.Item label="Supermarket" value="supermarket" />
        <Picker.Item label="ATM" value="atm" />
        <Picker.Item label="Stationary Store" value="ss" />
        <Picker.Item label="Jewllery" value="js" />
      </Picker>
      </View>
      <View style={{borderBottomColor: 'gray', borderBottomWidth: 2,height:100,alignItems:'center',justifyContent:'center'}}>        
        <Picker
        selectedValue={selectedValue3}
        style={styles.picker}
        onValueChange={(itemValue, itemIndex) => setSelectedValue3(itemValue)}>  
        <Picker.Item label="Choose the Category" value="Choose the Category" />
        <Picker.Item label="Bakery" value="bakery" />
        <Picker.Item label="Supermarket" value="supermarket" />
        <Picker.Item label="ATM" value="atm" />
        <Picker.Item label="Stationary Store" value="ss" />
        <Picker.Item label="Jewllery" value="js" />
      </Picker>
      </View>

      <View style={{alignItems:'center',paddingTop:50,paddingBottom:30}}>
        <Ripple
             onPress={() => navigation.navigate('shoplist')}
            style={styles.button}                   
             >                
        <Text style={{color:'white',fontSize:22}}> Search Shop </Text>
         </Ripple>
    </View>
        </ScrollView>
        </View>
        </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: 'white', 
        flex:1,
        width:'100%',
        height:'100%',
    },
    title: {
        fontWeight:'bold',
        fontSize:28,
        paddingTop:40,
        paddingLeft:30,
      },
      search:{
        alignItems:'center',
        marginTop:50,
        flexDirection: 'row',
        //justifyContent:'center',
        backgroundColor:'#e3e3e3',
        width:'75%',
        height:45,
        fontSize:15,
        borderRadius:30,
        paddingLeft:15,
        marginBottom:10,
        shadowColor: "#000",
        shadowOffset: {
	        width: 0,
	        height: 4,
        },
        shadowOpacity: 0.32,
        shadowRadius: 5.46,       
        elevation: 7,
      },
      picker:{
        width: "75%",  
        color: '#344953',
        transform: [
          { scaleX: 1.3 }, 
          { scaleY: 1.3 },
       ]      
    },
    button: {
      alignItems: 'center',
      backgroundColor: '#9378FF',
      padding:11,
      width:'65%',
      borderRadius:50,
      overflow:'hidden'
    }
})