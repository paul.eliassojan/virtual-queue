import React, { Component,useState} from "react";
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, Linking, Button, TouchableWithoutFeedback, Keyboard, KeyboardAvoidingView } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack'
import Ripple from 'react-native-material-ripple';
import { ScrollView } from 'react-native-gesture-handler';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import DateTimePicker from '@react-native-community/datetimepicker';
import { color } from "react-native-reanimated";

import DateTimePickerModal from "react-native-modal-datetime-picker";
export default function customer_info({ navigation }) {



    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
 
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };
    
    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };
    
    const handleDateConfirm = (date) => {
        Date(date);
        
        hideDatePicker();
    };

    const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
 
    const showTimePicker = () => {
        setTimePickerVisibility(true);
    };
    
    const hideTimePicker = () => {
        setTimePickerVisibility(false);
    };
    
    const handleTimeConfirm = (time) => {
        Time(time);
        hideTimePicker();
    };

    const [name,setname] = useState('name');
    const [phone,setphone] = useState('phone');
    const [date,Date] = useState('YY/MM/DD');
    const [time,Time] = useState('                 00:00');

    
    console.log(name);










    return (
        <KeyboardAvoidingView
        behavior={Platform.OS == "ios"? "padding" : "height"}
        keyboardVerticalOffset={Platform.OS === "ios" ? 64 : 0}
        style={{flex:1}}
      >    
        
        
        <View style={styles.container}>
        
            <View style={styles.topbar}>
                <TouchableOpacity style={styles.buttonclick} onPress={() => navigation.navigate('shoplist')}>
                    
                    <Image style={styles.backimage} source={require('../assets/Back.png')}/>
                    <Text style={styles.back}>Back</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.scrollcontainer}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View style={styles.mainarea}>
            
                
                <Text style={styles.name}>{navigation.getParam('name')}</Text>

                <View style={styles.locationbox}>
                    <Text style={styles.location}>Location</Text>
                    <View style={styles.placebox}>

                        <Text style={styles.place}>{navigation.getParam('place')}</Text>

                    </View>
                </View>
                <Text style={styles.details}>Enter the details</Text>
                <TextInput style={styles.input}
                placeholder='name'
                onChangeText = {(val) => setname(val)}
                />
                 
                 <TextInput style={styles.input}
                placeholder= 'phone'
                onChangeText = {(val) => setphone(val)}
                />

                <TouchableOpacity style={styles.buttonclick} onPress={showDatePicker}>
                <View style={styles.inputview}>
                
                    <Text style={styles.placeholder}>{date.toString().slice(0,15)}</Text>
                    <Image style={styles.dropdown} source={require('../assets/Icon.png')}/>
                   
                </View>
                </TouchableOpacity>

                <TouchableOpacity style={styles.buttonclick} onPress={showTimePicker}>
                <View style={styles.inputview}>
              
                    <Text style={styles.placeholder}>{time.toString().slice(17,22)}</Text>
                    <Image style={styles.dropdown} source={require('../assets/Icon.png')}/>
                </View>
                </TouchableOpacity >
                <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    mode="date"
                    onConfirm={handleDateConfirm}
                    onCancel={hideDatePicker}
                />
                <DateTimePickerModal
                    isVisible={isTimePickerVisible}
                    mode="time"
                    onConfirm={handleTimeConfirm}
                    onCancel={hideTimePicker}
                />
                
                
                <TouchableOpacity style={styles.submitbutton} onPress={() => navigation.navigate('confirmation')}>
                
                    <Text style={styles.submit}>Submit</Text>
                    
                
                </TouchableOpacity>


                
            
            </View>
            
            </TouchableWithoutFeedback>
            </ScrollView> 
            
        </View>
        
        
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({


    dropdown:{
        margin:'.5%',
        marginTop:'5%',
        flex:.3,
        marginRight:'2%'

    },

    placeholder:{
        margin:'.5%',
        marginTop:'4%',
        color:'grey',
        flex:9.7
    },

    input:{
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor:'#FFFFFF',
        width:'90%',
        height:52,
        fontSize:17,
        borderRadius:10,
        margin:'5%',
        paddingLeft:10,
        shadowColor:'#000000',
        shadowOffset: {
            width: 0,
            height: -1
            },
        shadowRadius: 1,
        shadowOpacity: 1,
        elevation: 5
    },


      inputview:{
        borderColor: 'gray',
        borderWidth: 1,
        backgroundColor:'#FFFFFF',
        width:'90%',
        height:52,
        fontSize:17,
        flexDirection:'row',
        borderRadius:10,
        margin:'5%',
        paddingLeft:10,
        shadowColor:'#000000',
        shadowOffset: {
            width: 0,
            height: -1
            },
        shadowRadius: 1,
        shadowOpacity: 1,
        elevation: 5
      },

    container:{
        backgroundColor: 'white', 
        flex:1,
        width:'100%',
        height:'100%',
    },
    scrollcontainer:{
        backgroundColor: 'white',
        width:'100%',
        flex:9.2,
        flexGrow:9.2,
    },



    details:{
        color:'black',
        fontSize: RFValue(16, 720),
        marginLeft:'4%',
        marginTop:'2%'
    },
    place:{
        color:'white',
        fontSize:RFValue(16, 720),
        margin:'2%',
    },

    placebox:{
        alignItems:'center'
    },

    location:{
        color:'white',
        fontSize:RFValue(18, 720),
        textAlign:'left',
        margin:'2%',
        textDecorationLine: 'underline',
    },

    locationbox:{
        backgroundColor:'#9279FE',
        height:100,
        borderBottomLeftRadius:0,
        borderBottomRightRadius:8,
        borderTopRightRadius:8,
        borderTopLeftRadius:8,
        marginTop:'2%',
        marginLeft:'2%',
        marginRight:'2%',
        marginBottom:'2%'
    },


    submitbutton:{
        backgroundColor:'#9279FE',
        height:50,
        borderRadius:60,
        alignItems:'center',
        marginTop:'2%',
        marginLeft:'20%',
        marginRight:'20%',
        marginBottom:30
    },

    submit:{
        color:'white',
        fontSize:RFValue(18, 720),
        textAlign:'left',
        margin:'5%',
    },
    

    buttonclick:{
        flexDirection: 'row',
        alignItems:'center',
    },

    backimage:{
        marginLeft:'2%'
    },

    mainarea:{
        backgroundColor: 'white',
        width:'100%',
        flex:9.2
        
    },
    box:{
        backgroundColor: 'white',
        width:'100%',
    },
   
    topbar:{
        flexDirection: 'row',
        height:'10%',
        backgroundColor:"#F2F2F2",
        alignItems:'center',
    },

    name:{
        fontSize:RFValue(25, 720),
        fontWeight: 'bold',
        color:'black',
        margin:10
    },
    
    back:{
        fontSize:RFValue(16, 720),
        color:'grey',
        textAlign:'left',
        paddingLeft:'1%'
    }
})