import { createStackNavigator } from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
import Vendor from '../components/vendor';
import Login from '../components/login';
import customer_info from '../components/customer_info';
import Vendor_reg from '../components/vendor_reg';
import Vendor_reg2 from '../components/vendor_reg2';
import shoplist from '../components/shoplist';
import Customer from '../components/customer';
import Admin from '../components/screens/admin';
import confirmation from '../components/confirmation';

import React from 'react';

const HomeNav=createStackNavigator({
    
    Home: {
        screen: Login,
        navigationOptions: {
          headerShown: false,
        }
    },
    customer_info: {
      screen: customer_info,
      navigationOptions: {
        headerShown: false,
      }
    },
    Vendor:{
        screen:Vendor,
        navigationOptions: {
          headerShown: false,
        }
    },
    Vendor_reg:{
      screen:Vendor_reg,
      navigationOptions: {
        headerShown: false,
      }
    },
        Vendor_reg2:{
      screen:Vendor_reg2,
      navigationOptions: {
        headerShown: false,
      }
    },
    Customer:{
      screen:Customer,
      navigationOptions: {
        headerShown: false,
      }
    },
    Admin:{
      screen:Admin,
    },
    shoplist:{
      screen:shoplist,
      navigationOptions: {
        headerShown: false,
      }
    },
    confirmation: {
      screen:confirmation,
      navigationOptions: {
        headerShown: false,
      }
    }

});

export default createAppContainer(HomeNav);